﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace My_Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] randomWordArray = { "foo", "bar", "baz", "herp", "derp", "gazerp" };
            ChaosArray<string> stringArray = new ChaosArray<string>(randomWordArray.Length);
            foreach (string word in randomWordArray)
            {
                try
                {
                    stringArray.SetItem(word);
                } catch (IndexNotEmptyException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            Console.WriteLine("String array:");
            stringArray.PrintArray();

            int[] numbersArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            ChaosArray<int> intArray = new ChaosArray<int>(numbersArray.Length);
            foreach (int number in numbersArray)
            {
                try
                {
                    intArray.SetItem(number);
                }
                catch (IndexNotEmptyException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            Console.WriteLine("Int array:");
            intArray.PrintArray();

            Console.WriteLine("\nTesting foreach numbers");
            foreach (int number in intArray)
            {
                Console.WriteLine(number);
            }
            Console.WriteLine("\nTesting foreach strings");
            foreach (string str in stringArray)
            {
                Console.WriteLine(str);
            }
        }
    }

    public class ChaosArray<T>: IEnumerable<T>
    {
        node[] array;

        public ChaosArray(int size)
        {
            array = new node[size];
        }
        struct node
        {
            public T value;
            public bool initialized;
        }
        public T this[int i]
        {
            get => GetItem();
            set => SetItem(value);
        }

        public T GetItem()
        {
            int index = GetRandomArrayIndex();

            node item = array[index];
            if (!item.initialized)
            {
                throw new IndexNotEmptyException(
                    $"Error: Can not get value of index. {typeof(T).Name}[{index}] is not initialized.");
            }   else
            {
                return item.value;
            }
        }

        public void SetItem(T newValue)
        {
            int index = GetRandomArrayIndex();
            if (array[index].initialized)
            {
                throw new IndexNotEmptyException();
            }
            else
            {
                array[index].value = newValue;
                array[index].initialized = true;
            }
        }
        private int GetRandomArrayIndex()
        {
            Random random = new Random();
            return random.Next(0, array.Length - 1);
        }

        public void PrintArray()
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine($"{i}: {array[i].value}");
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            T[] results = array.Where(e => e.initialized).Select(e => e.value).ToArray();

            Random rand = new Random();
            foreach (T item in results.OrderBy(i => rand.Next())) yield return item;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

    }

    public class IndexNotEmptyException: Exception
    {
        public IndexNotEmptyException() : base("Error: Index on array not empty.") {}
        public IndexNotEmptyException(string message) : base(message) {}
    }
}
